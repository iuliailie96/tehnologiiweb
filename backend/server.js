const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const sequelize = new Sequelize('GoogleBooks', 'root', '', {
  dialect : 'mysql',
  define : {
    timestamps : false
  }
})

const Book = sequelize.define('book', {
  author : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }
  },
  title : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }  
  }
})

const Message = sequelize.define('message', {
  title  : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }
  },
  content : {
    type : Sequelize.TEXT,
    allowNull : false,
    validate : {
      len : [10,1000]
    }
  },
  timestamp : {
    type : Sequelize.DATE,
    allowNull : false,
    defaultValue : Sequelize.NOW
  }
})

Book.hasMany(Message)

const app = express()
app.use(bodyParser.json())
app.use(express.static('../frontend/build'))

app.get('/create', (req, res, next) => {
  sequelize.sync({force : true})
    .then(() => res.status(201).send('tables created'))
    .catch((error) => next(error))
})

app.get('/Books' , (req, res, next) => {
  Book.findAll()
    .then((books) => res.status(200).json(books))
    .catch((error) => next(error))
})

app.post('/Books' , (req, res, next) => {
  Book.create(req.body)
    .then(() => res.status(201).send('created books'))
    .catch((error) => next(error))
})

app.get('/Books/:id' , (req, res, next) => {
  Book.findById(req.params.id, {include : [Message]})
    .then((book) => {
      if (book){
        res.status(200).json(book)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))
})

app.put('/Books/:id' , (req, res, next) => {
  Book.findById(req.params.id)
    .then((book) => {
      if (book){
        return book.update(req.body, {fields : ['author', 'title']})
      }
      else{
        res.status(404).send('book not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('book modified')
      }
    })
    .catch((error) => next(error))
})

app.delete('/Books/:id' , (req, res, next) => {
    Book.findById(req.params.id)
    .then((book) => {
      if (book){
        return book.destroy()
      }
      else{
        res.status(404).send('book not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('book modified')
      }
    })
    .catch((error) => next(error))
})

app.get('/Books/:aid/messages' , (req, res, next) => {
  Book.findById(req.params.aid)
    .then((book) => {
      if (book){
        return book.getMessages()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then((messages) => {
      if (!res.headersSent){
        res.status(200).json(messages)
      }
    })
    .catch((error) => next(error))
})

app.post('/Books/:aid/messages' , (req, res, next) => {
  Book.findById(req.params.aid)
    .then((book) => {
      if (book){
        let message = req.body
        message.book_id = book.id
        return Message.create(message)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('created')
      }
    })
    .catch((error) => next(error))  
})

app.get('/Books/:aid/messages/:mid' , (req, res, next) => {
  Message.findById(req.params.mid, {
      where : {
        book_id : req.params.aid
      }
    })
    .then((message) => {
      if (message){
        res.status(200).json(message)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))  
})

app.put('/Books/:aid/messages/:mid' , (req, res, next) => {
  Message.findById(req.params.mid)
    .then((message) => {
      if (message){
        return message.update(req.body, {fields : ['title', 'content']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})
app.delete('/Books/:aid/messages/:mid' , (req, res, next) => {
  Message.findById(req.params.mid)
    .then((message) => {
      if (message){
        return message.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.use((err, req, res, next) =>{
  console.warn(err)
  res.status(500).send('some error')
})

app.listen(8080)