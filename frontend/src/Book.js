import React, {Component} from 'react'

class Book extends Component{
  constructor(props){
    super(props)
    this.state = {
      BookAuthor : '',
      BookTitle : '',
      isEditing : false
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.author] : event.target.value
      })
    }
  }
  componentDidMount(){
    this.setState({
      BookAuthor : this.props.book.author,
      BookTitle : this.props.book.title
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      isEditing : false,
      BookAuthor : nextProps.book.author,
      BookTitle : nextProps.book.title
    })
  }
  render(){
    if (!this.state.isEditing){
      return (
        <div>
          {this.props.book.author} - {this.props.book.title}
          <input type="button" value="edit" onClick={() => this.setState({isEditing : true})} />
          <input type="button" value="delete" onClick={() => this.props.onDelete(this.props.book.id)} />
          <input type="button" value="select" onClick={() => this.props.onSelect(this.props.book.id)} />
        </div>  
      )      
    }
    else{
      return (
        <div>
          <input type="text" author="BookAuthor" onChange={this.handleChange} value={this.state.BookAuthor} />
          <input type="text" author="BookTitle" onChange={this.handleChange} value={this.state.BookTitle} />
          <input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
          <input type="button" value="save" onClick={() => this.props.onSave(this.props.book.id, {author : this.state.BookAuthor, title : this.state.BookTitle})} />
        </div>  
      )
    }
  }
}

export default Book