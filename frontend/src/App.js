import React, { Component } from 'react'
import BookStore from './BookStore'
import Book from './Book'
import BookForm from './BookForm'
import {EventEmitter} from 'fbemitter'
import logo from './icon.ico';
import './App.css';

const ee = new EventEmitter()
const store = new BookStore(ee)

function addBook(Book){
  store.createOne(Book)
}

function deleteBook(id){
  store.deleteOne(id)
}

function saveBook(id, Book){
  store.saveOne(id, Book)
}

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      books : [],
      detailsFor : 0,
      selected : null
    }
    this.selectBook = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_BOOK_LOAD', () => {
        this.setState({
          selected : store.selected,
          detailsFor : store.selected.id
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('BOOK_LOAD', () => {
      this.setState({
        books : store.content
      })
    })
  }
  render() {
    if (!this.state.detailsFor){
      return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo"/>
            <h1 className="App-title">Google Books</h1>
          </header>
          <div id="container">
            <div>
              <h3>A list of books</h3>
              {
                this.state.books.map((a) => 
                  <Book book={a} key={a.id} onDelete={deleteBook} onSave={saveBook} onSelect={this.selectBook} />
                )
              }
            </div>
            <div>
              <h3>Add one book</h3>
              <BookForm onAdd={addBook} />
            </div>
          </div>
        </div>
      );      
    }
else{
      return (
        <div>
          Details for: {this.state.selected.name}
          <input type="button" value="back" onClick={() => this.setState({detailsFor : 0})} />
        </div>
      )
    }
  }
}

export default App;
