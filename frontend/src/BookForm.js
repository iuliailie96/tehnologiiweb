import React, {Component} from 'react'

class BookForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      BookAuthor : '',
      BookTitle : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render(){
    return (
      <div>
        Author : <input type="text" name="BookAuthor" onChange={this.handleChange}/>
        Title: <input type="text" name="BookTitle" onChange={this.handleChange} />
        <input type="button" value="add" onClick={() => this.props.onAdd({author : this.state.BookAuthor, title : this.state.BookTitle})} />
      </div>  
    )
  }
}

export default BookForm