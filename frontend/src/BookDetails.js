import React, {Component} from 'react'
import MessageStore from '.MessageStore'
import {EventEmitter} from 'fbemitter'
import Message from './Message'
import MessageForm from './MessageForm' 

const ee = new EventEmitter()
const store = new MessageStore(ee)

class BookDetails extends Component{
  constructor(props){
    super(props)
    this.state = {
      messages : []
    }
    this.addMessage = (message) => {
      store.addOne(this.props.book.id, message)
    }
  }
  componentDidMount(){
    store.getAll(this.props.book.id)
    ee.addListener('MESSAGE_LOAD',  () => {
      this.setState({
        messages : store.content
      })
    })
  }
  render(){
    return (
      <div>
        I am the details page for {this.props.book.author} with his book {this.props.book.title}
        <h3>List of messages</h3>
          {
            this.state.messages.map((m) => 
              <Message message={m} />
            )
          }        
        <h3>Add another one </h3>
        <MessageForm onAdd={this.addMessage} />
      </div>  
    )
  }
}

export default BookDetails



