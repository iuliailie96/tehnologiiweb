import axios from 'axios'

const SERVER = 'https://seminar-web-iuliailie.c9users.io'

class BookStore{
  constructor(ee){
    this.content = []
    this.ee = ee
    this.selected = null
  }
  getAll(){
    axios(SERVER + '/Books')
      .then((response) => {
        this.content = response.data
        this.ee.emit('BOOK_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  createOne(Book){
    axios.post(SERVER + '/Books', Book)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/Books/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, Book){
    axios.put(SERVER + '/Books/' + id, Book)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  getOne(id){
    axios(SERVER + '/Books/' + id)
      .then((response) => {
        this.selected = response.data
        this.ee.emit('SINGLE_BOOK_LOAD')
      })
      .catch((error) => console.warn(error))
  }
}

export default BookStore