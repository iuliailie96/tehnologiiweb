import React, { Component } from 'react'
import Book from './Book'
import BookForm from './BookForm'
import BookStore from '.BookStore'
import {EventEmitter} from 'fbemitter'
import BookDetails from './BookDetails'

const ee = new EventEmitter()
const store = new BookStore(ee)

function addBook(book){
  store.addOne(book)
}

function deleteBook(id){
  store.deleteOne(id)
}

function saveBook(id, book){
  store.saveOne(id,book)
}

class BookList extends Component {
  constructor(props){
    super(props)
    this.state = {
      books : [],
      detailsFor : 0,
      selected : null
    }
    this.selectBook = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_BOOK_LOAD', () => {
        this.setState({
          detailsFor : store.selected.id,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('BOOK_LOAD', () => {
      this.setState({
        book : store.content
      })
    })
  }
  render() {
    if (!this.state.detailsFor){
      return (
        <div>
          <div>
            {this.state.book.map((a) =>
              <Book book={a} key={a.id} onDelete={deleteBook} onSave={saveBook} onSelect={this.selectBook} />
            )}
          </div>
          <div>
            <BookForm onAdd={addBook}/>
          </div>
        </div>
      )      
    }
    else{
      return (
        <div>
          <BookDetails book={this.state.selected} />
          <input type="button" value="back" onClick={() => this.setState({detailsFor : 0})}/> 
        </div>  
      )
    }
  }
}

export default BookList






